import os, fnmatch
from javax.swing import JList, JOptionPane, JScrollPane, JFrame, JLabel, JButton, JTextField, JComboBox, JTable
from os import path
from java.awt import Desktop
from java.io import File
from com.sun.org.apache.xalan.internal.xsltc.compiler import If

data=[]

def rechercher(event):
    data=[]
    path= txt1.getText()
    quoi= txt2.getText()
    if((path=="") or (quoi=="")):
        JOptionPane.showMessageDialog(frame,"Veuillez remplir tous les champs.")
        frame.setVisible(True)
    else:    
        if (cb.getSelectedItem()=="Tout"):
            motCle="*"+quoi+"*"
        if (cb.getSelectedItem()=="Fichier audio"):
            motCle="*"+quoi+"*.mp3"
        if (cb.getSelectedItem()=="Fichier video"):
            motCle="*"+quoi+"*.mp4"
        if (cb.getSelectedItem()=="Document"):
            motCle="*"+quoi+"*.pdf"
        if (cb.getSelectedItem()=="Librairie python"):
            motCle="*"+quoi+"*.py"
        
        try:
            listFIles=os.listdir(path)
            for file1 in listFIles:
                if fnmatch.fnmatch(file1, motCle):
                    data.append(file1)
            
            print("recherche en cours...")
            
            if(data==[] and path!="" and quoi!=""):
                JOptionPane.showMessageDialog(frame,"Fichier introuvable...")
                frame.setVisible(True)
            
            if(data!=[] and path!="" and quoi!="" ): 
                
                def listSelect(event):
                   index = lst.selectedIndex
                   if(JOptionPane.showConfirmDialog(None, "Voulez vous ouvrir le fichier '" + data[index] + "' ?", "Ouvrir",JOptionPane.YES_NO_OPTION) == JOptionPane.OK_OPTION):
                       desktop = Desktop.getDesktop()
                       file = File("test/" + data[index])
                       desktop.open(file)
                   frame.setVisible(True)
                   
                lst=JList(data, valueChanged = listSelect)
                
                js.setViewportView(lst)
                frame.add(js)
                frame.setVisible(True)
                
        except Exception, err:
            print(err)
            JOptionPane.showMessageDialog(frame,"Le dossier est introuvable")
            frame.setVisible(True)
            
frame = JFrame("Jython M1")
frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE)
frame.setLocation(100,100)
frame.setSize(550,300)
frame.setLayout(None)
frame.setResizable(False)

lbl2= JLabel("Chemin:")
lbl2.setBounds(10,10,60,20)
txt1 = JTextField(10)
txt1.setBounds(65,10,300,20)

lbl3 = JLabel("Quoi:")
lbl3.setBounds(10, 35,60,20)
txt2 = JTextField(10)
txt2.setBounds(65, 35, 300,20)

lbl4 = JLabel("Type:")
lbl4.setBounds(10,60,60,20)

cb= JComboBox([
    "Fichier audio",
    "Fichier video",
    "Document",
    "Librairie python",
    "Tout"
    ])
cb.setBounds(65,60, 300, 20)

btn = JButton("Rechercher", actionPerformed = rechercher)
btn.setBounds(370,10,160,45)

colone= []

js=JScrollPane()
js.setBounds(10,90,520, 160)

lst = JList()

frame.add(js)
frame.add(cb)
frame.add(txt1)
frame.add(lbl2)
frame.add(txt2)
frame.add(lbl3)
frame.add(lbl4)
frame.add(btn)

frame.setVisible(True)
